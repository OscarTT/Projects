import React, { Component } from "react";
import "./App.css";
import MovieRow from "./MovieRow";
import $ from "jquery";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.performSearch("");
  }

  performSearch(searchTerm) {
    const urlString = "https://api.themoviedb.org/3/search/movie?api_key=44ec7bceaad09564f91595cfdd54603d&query=" + searchTerm;
    $.ajax({
      url: urlString,
      success: searchResults => {
        const results = searchResults.results;
        var movieRows = [];
        results.forEach(movie => {
          movie.poster_src =
            "https://image.tmdb.org/t/p/w185" + movie.poster_path;
          const movieRow = <MovieRow key={movie.id} movie={movie} />;
          movieRows.push(movieRow);
        });
        this.setState({ rows: movieRows });
      },
      error: (xhr, status, err) => {}
    });
  }
  searchChangeHandler(event) {
    const boundObject = this;
    const searchTerm = event.target.value;
    boundObject.performSearch(searchTerm);
  }

  render() {
    return (
      <div className="App">
        <table className="titleBar">
          <tbody>
            <tr>
              <td>
                <img alt="app logo" src="popcorn.png" />
              </td>
              <td>
                <h1>MoviesDB Search</h1>
              </td>
            </tr>
          </tbody>
        </table>
        <input
          className="inputBox"
          placeholder="Search For Movie"
          onChange={this.searchChangeHandler.bind(this)}
        />
        {this.state.rows}
      </div>
    );
  }
}

export default App;
