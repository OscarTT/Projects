import React from "react"

export default ({ id, info, handleFavorite }) => (
  <li className={info.sex} onClick={() => handleFavorite(id)}>
    {info.name}
  </li>
)
