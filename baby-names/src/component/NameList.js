import React from "react"
import Name from "./Name"

export default ({ data, filter, favorites, addFavorite }) => {
  const input = filter.toLowerCase()
  const names = data
    .filter((person, i) => {
      return (
        favorites.indexOf(person.id) === -1 &&
        !person.name.toLowerCase().indexOf(input)
      )
    })
    .map((person, i) => {
      return (
        <Name
          id={person.id}
          key={i}
          info={person}
          handleFavorite={id => addFavorite(id)}
        />
      )
    })

  return <ul>{names}</ul>
}
