import React from "react"
import Name from "./Name"

export default ({ data, favorites, deleteFavorite }) => {
  const hasFavorites = favorites.length > 0
  const favList = favorites.map((fav, i) => {
    return (
      <Name
        id={i}
        key={i}
        info={data[fav]}
        handleFavorite={id => deleteFavorite(id)}
      />
    )
  })

  return (
    <div className="favorites">
      <h4>
        {hasFavorites ? "Your ShortList" : "Click on name to shortlist it..."}
      </h4>

      <ul>{favList}</ul>
      {hasFavorites && <hr />}
    </div>
  )
}
