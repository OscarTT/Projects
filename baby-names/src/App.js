import React, { Component } from "react";
import NameList from "./component/NameList";
import ShortList from "./component/ShortList";
import Search from "./component/Search";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: "",
      favorites: []
    };
  }

  filterUpdate(value) {
    this.setState({
      filterText: value
    });
  }

  addFavorite(id) {
    const newSet = this.state.favorites.concat([id]);
    this.setState({
      favorites: newSet
    });
  }

  deleteFavorite(id) {
    const { favorites } = this.state;
    const newList = [...favorites.slice(0, id), ...favorites.slice(id + 1)];
    this.setState({
      favorites: newList
    });
  }

  render() {
    const hasSearch = this.state.filterText.length > 0;
    return (
      <div>

        <header>
          <Search
            filterVal={this.state.filterText}
            filterUpdate={this.filterUpdate.bind(this)}
          />
        </header>

        <main>
        
          <ShortList
            data={this.props.data}
            favorites={this.state.favorites}
            deleteFavorite={this.deleteFavorite.bind(this)}
          />

          <NameList
            data={this.props.data}
            filter={this.state.filterText}
            favorites={this.state.favorites}
            addFavorite={this.addFavorite.bind(this)}
          />

          {hasSearch && (
            <button onClick={this.filterUpdate.bind(this, "")}>
              Clear Search
            </button>
          )}
          <div className="credit">
            <a href="https:www.yahoo.com">
              Source of names list: Yahoo - Top Baby Names
            </a>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
