import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
    Button,
    Col,
    ControlLabel,
    FormGroup,
    FormControl,
    Row
} from "react-bootstrap";

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                email: "",
                password: "",
                errors: {}
            }
        };
    }
    handleChange(e) {
        const formState = Object.assign({}, this.state.form);
        formState[e.target.name] = e.target.value;
        this.setState({ form: formState });
    }

    render() {
        const { form, errors } = this.state;
        const { firstName, lastName, email, password } = form;

        return (
            <form>
                <Row>
                    <Col xs={6}>
                        <FormGroup>
                            <ControlLabel id="email" />
                            <FormControl
                                type="text"
                                name="email"
                                placeholder="Email"
                                value={email}
                                onChange={this.handleChange.bind(this)}
                            />
                        </FormGroup>
                    </Col>
                </Row>

                <Row>
                    <Col xs={6}>
                        <FormGroup>
                            <ControlLabel id="password" />
                            <FormControl
                                type="password"
                                name="password"
                                placeholder="Password"
                                value={this.state.form.password}
                                onChange={this.handleChange.bind(this)}
                            />
                        </FormGroup>
                    </Col>
                </Row>

                <Row id="log_in_row">
                    <Col xs={6} id="log_in_col">
                        <Link to="/games">
                            <Button id="logInbtn">Log in</Button>
                        </Link>
                        <Link to="/signup">
                            <Button id="logInbtn">Sign Up</Button>
                        </Link>
                    </Col>
                </Row>

                <Row>
                    <Col xs={6} id="orjust">
                        <p>OR</p>
                    </Col>
                </Row>

                <Row id="play_row">
                    <Col xs={6} id="play_col">
                        <Link to="/games">
                            <Button id="playbtn">PLAY NOW !</Button>
                        </Link>
                    </Col>
                </Row>
            </form>
        );
    }
}
export default SignIn;
