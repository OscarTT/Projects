import React, { Component } from 'react'
import SignIn from '../components/signIn'
import '../App.css';

class LandingPageView extends Component {
  render() {
    return (
        <div className="home-bg">
            <div>
            </div>
            <div className="container">
                <div className="signin">
                    <h3>TRIVIA</h3>
                        <SignIn />
                </div>
            </div>
        </div>

    );
  }
}

export default LandingPageView;
