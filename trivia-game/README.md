## Trivia Game
A trivia game that supports multi-player. Players must be logged in within 15 seconds to play against one another. API contains multiple choice and true or false questions of easy,medium, hard difficulty.

## Screenshots
<img src="trivia_game.gif?raw=true" width="100%" height="100%">


## Build status
Deployed on https://devx-trivia.herokuapp.com/
Log-in, sign-up form not required currently

## Tech/framework used
Javascript, REACT, Express, CSS, HTML, API


## API Reference

https://opentdb.com/api_config.php
